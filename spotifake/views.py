from .app import app, db
from .models import *
from hashlib import sha256
from flask.ext.wtf import Form
from wtforms.validators import DataRequired
from flask.ext.login import login_user, current_user, logout_user, login_required
from wtforms import StringField, HiddenField, IntegerField, PasswordField, TextField, validators
from flask import render_template, url_for, redirect, request, flash
from wtforms.ext.sqlalchemy.fields import QuerySelectMultipleField

@app.context_processor #fait passer des vars à toutes les pages
def inject_var():
	plays = None
	gu = None
	if current_user.is_authenticated:
		plays = get_playlistUser(current_user)
		gu = god_user(current_user)
	return dict(formu = SearchForm(),
				plays = plays,
				god_user = gu)

@app.route("/")
def home():
	return render_template(
		"home.html",
		title = "Spotifake",
		albums = get_sample())



# **************************
# ARTIST
# **************************
@app.route("/artists")
@login_required
def artists_page():
	return render_template("artists.html",
							title = "Artists",
							artists = get_all_artists())



@app.route("/artist/<int:id>")
@login_required
def one_artist(id): 
	return render_template("one_artist.html",
							artist = get_artist(id),
							albums = get_artistAlbums(id))

@app.route("/artist/suppr/<int:id>", methods=("GET","POST"))
def deleteArtist(id):
	if god_user(current_user):
		del_artist(id)
		return render_template("artists.html",
								title = "Artists",
								artists = get_all_artists())
	else:
		return redirect(url_for('home'))

# **************************
# ALBUM
# **************************
@app.route("/albums")
@login_required
def albums_page():
	return render_template("albums.html",
							title = "Albums",
							albums = get_all_albums())


@app.route("/album/<int:id>")
@login_required
def one_album(id):
	a = get_album(id)
	return render_template("one_album.html",
							album = a,
							artist = get_artist(a.artist_id),
							sw = get_sw(a.sw_id),
							playlists = get_playlistUser(current_user))



# non fonctionnelle
@app.route("/album/suppr/<int:id>", methods=("GET","POST"))
def deleteAlbum(id):
	delAlbum(id)
	return render_template("albums.html",
							title = "Albums",
							albums = get_all_albums())


# **************************
# GENRE
# **************************

@app.route("/genre/<int:id>")
@login_required
def one_genre(id):
	return render_template("one_genre.html",
							genre = get_genre(id))

@app.route("/genres")
@login_required
def genres_page():
	return render_template("genres.html",
							title = "Genres",
							genres = get_all_genre())


# non fonctionnelle
@app.route("/genre/suppr/<int:id>", methods=("GET","POST"))
def deleteGenre(id):
	del_genre(id)
	return render_template("genres.html",
							title = "Genres",
							genres = get_all_genre())


# **************************
# SW
# **************************

@app.route("/sw/<int:id>")
@login_required
def one_sw(id):
	return render_template("one_sw.html",
							sw = get_sw(id),
							albums = get_swAlbums(id))

@app.route("/sw")
@login_required
def sw_page():
	return render_template("sw.html",
							title = "Songwriters",
							sw = get_all_sw())

@app.route("/sw/suppr/<int:id>", methods=("GET","POST"))
def deleteSW(id):
	if god_user(current_user):
		del_sw(id)
		return render_template("sw.html",
								title = "Songwriters",
								sw = get_all_sw())
	else:
		return redirect(url_for('home'))


# **************************
# Edition ALBUM
# **************************
class AlbumForm(Form):
	id = HiddenField('id')
	title = StringField("Title")
	release_year = StringField("Year")
	artist = StringField("Artist")
	sw = StringField("Songwriter")
	genre = QuerySelectMultipleField('Genre', query_factory=lambda: db.session.query(Genre), get_pk=lambda a: a.id, get_label=lambda a: a.name)

@app.route("/edit/album/<int:id>")
@login_required
def edit_album(id):
	if god_user(current_user):
		a = get_album(id)
		f = AlbumForm(id = a.id, title = a.title, release_year = a.release_year, artist = get_artist(a.artist_id).name, sw = get_sw(a.sw_id).name, genre = a.genre)
		return render_template("edit_album.html",
			album = a,
			form = f,
			formu = SearchForm())
	else:
		return redirect(url_for('home'))

@app.route("/save/album/", methods=("POST", "GET"))
@login_required
def save_album():
	a = None
	f = AlbumForm()
	if f.validate_on_submit():
		id = int(f.id.data)
		a = get_album(id)
		a.title = f.title.data
		a.release_year = f.release_year.data

		z = None
		if testArtist(f.artist.data) == None:
			z = Artist(name=f.artist.data)
			db.session.add(z)
			db.session.commit()
		else:
			z = testArtist(f.artist.data)
		a.artist_id = z.id

		sw0 = None
		if testSw(f.sw.data) == None:
			swO = Songwriter(name=f.sw.data)
			db.session.add(swO)
		else:
			swO =testSw(f.sw.data)
		a.sw_id = swO.id

		genres = f.genre.raw_data
		a.genre = []
		for g in genres:
			genre = db.session.query(Genre).get(g)
			if not genre:
				genre = Genre(g.name)
				db.session.add(genre)
			a.genre.append(genre)

		db.session.commit()
		return redirect(url_for('one_album', id = a.id))
	a = get_album(int(f.id.data))
	return render_template("edit_album.html",
							album = a,
							form   = f,
							formu = SearchForm())


def testArtist(name):
	liste = get_all_artists()
	artr = None
	for art in liste:
		if art.name.lower() == name.lower():
			artr = art
	return artr

def testSw(name):
	liste = get_all_sw()
	sww = None
	for s in liste:
		if s.name.lower() == name.lower():
			sww = s
	return sww


# **************************
# Edition Artist
# **************************
class ArtistForm(Form):
	id = HiddenField('id')
	name = StringField('Nom', validators = [DataRequired()])


@app.route("/edit/artist/<int:id>")
@login_required
def edit_artist(id):
	if god_user(current_user):
		a = get_artist(id)
		f = ArtistForm(id = a.id, name = a.name)
		return render_template("edit_artist.html",
			artist = a,
			form = f)
	else:
		return redirect(url_for('home'))


@app.route("/save/artist/", methods=("POST", "GET"))
@login_required
def save_artist():
	a = None
	f = ArtistForm()
	if f.validate_on_submit():
		id = int(f.id.data)
		a = get_artist(id)
		a.name = f.name.data
		db.session.commit()
		return redirect(url_for('one_artist', id = a.id))
	a = get_artist(int(f.id.data))
	return render_template("edit_artist.html",
							artist = a,
							form   = f)


# **************************
# Create
# **************************


class CreateArtist(Form):
	name = StringField('Name', validators=[DataRequired()])

@app.route("/edit/create/artist/", methods=("GET","POST"))
@login_required
def create_artist():
	f = CreateArtist()
	if not inDBArtist(f.name.data) and len(f.name.data)>0:
		a = Artist(name = f.name.data)
		db.session.add(a)
		db.session.commit()
	return render_template("user_page.html",
							playlist = get_playlistUser(current_user),
							form = NewPlaylist(),
							artF = CreateArtist(),
							swF = CreateSW(),
							genF = CreateGenre(),
							albF = CreateAlbum())
##

class CreateSW(Form):
	name = StringField('Name', validators=[DataRequired()])

@app.route("/edit/create/sw/", methods=("GET","POST"))
@login_required
def create_sw():
	f = CreateSW()
	if not inDBSW(f.name.data) and len(f.name.data)>0:
		a = Songwriter(name = f.name.data)
		db.session.add(a)
		db.session.commit()
	return render_template("user_page.html",
							playlist = get_playlistUser(current_user),
							form = NewPlaylist(),
							artF = CreateArtist(),
							swF = CreateSW(),
							genF = CreateGenre(),
							albF = CreateAlbum())

##

class CreateGenre(Form):
	name = StringField('Name', validators=[DataRequired()])

@app.route("/edit/create/genre/", methods=("GET","POST"))
@login_required
def create_genre():
	f = CreateGenre()
	if not inDBGenre(f.name.data) and len(f.name.data)>0:
		a = Genre(name = f.name.data)
		db.session.add(a)
		db.session.commit()
	return render_template("user_page.html",
							playlist = get_playlistUser(current_user),
							form = NewPlaylist(),
							artF = CreateArtist(),
							swF = CreateSW(),
							genF = CreateGenre(),
							albF = CreateAlbum())

##

class CreateAlbum(Form):
	title = StringField('Title', validators=[DataRequired()])
	release_year = IntegerField("Year")
	artist = StringField("Artist")
	sw = StringField("Songwriter")
	genre = QuerySelectMultipleField('Genre', query_factory=lambda: db.session.query(Genre), get_pk=lambda a: a.id, get_label=lambda a: a.name)

@app.route("/edit/create/album/", methods=("GET","POST"))
@login_required
def create_album():
	f = CreateAlbum()
	if not inDBAlbums(f.title.data) and len(f.title.data)>0:
		a = Album(title = f.title.data,
				release_year = f.release_year.data)
		z = None
		if testArtist(f.artist.data) == None:
			z = Artist(name=f.artist.data)
			db.session.add(z)
			db.session.commit()
		else:
			z = testArtist(f.artist.data)
		a.artist_id = z.id

		swO = None
		if testSw(f.sw.data) == None:
			swO = Songwriter(name=f.sw.data)
			db.session.add(swO)
			db.session.commit()
		else:
			swO =testSw(f.sw.data)
		a.sw_id = swO.id

		genres = f.genre.raw_data
		a.genre = []
		for g in genres:
			genre = db.session.query(Genre).get(g)
			if not genre:
				genre = Genre(g.name)
				db.session.add(genre)
			a.genre.append(genre)
		db.session.commit()
	return render_template("user_page.html",
							playlist = get_playlistUser(current_user),
							form = NewPlaylist(),
							artF = CreateArtist(),
							swF = CreateSW(),
							genF = CreateGenre(),
							albF = CreateAlbum())



# **************************
# Login
# **************************
class LoginForm(Form):
	username = StringField('Username')
	password = PasswordField('Password')
	next = HiddenField()

	def get_authenticated_user(self):
		user = User.query.get(self.username.data)
		if user is None:
			return None
		m = sha256()
		m.update(self.password.data.encode())
		passwd = m.hexdigest()
		return user if passwd == user.password else None


@app.route("/login/", methods=("GET", "POST",))
def login():
	f = LoginForm()
	if not f.is_submitted():
		f.next.data = request.args.get("next")
	elif f.validate_on_submit():
		user = f.get_authenticated_user()
		if user:
			login_user(user)
			next = f.next.data or url_for("home")
			return redirect(next)
	return render_template("login.html",
							form = f)


@app.route("/logout/")
def logout():
	logout_user()
	return redirect(url_for('home'))


class RegistrationForm(Form):
    username = StringField('Username')
    password = PasswordField('New Password')


@app.route('/register', methods=['GET', 'POST'])
def register():
	form = RegistrationForm()
	if form.validate_on_submit():
		if existing_user(form.username.data) == None: # verification si l'username existe déjà
			m = sha256()
			m.update(form.password.data.encode())
			passwd = m.hexdigest()
			user = User(username = form.username.data, 
						password = passwd,
						role = "lambda")
			db.session.add(user)
			db.session.commit()
			return redirect(url_for('home'))
	return render_template('register.html', 
    						form=form)



# **************************
# Search
# **************************
class SearchForm(Form):
	recherche = StringField('Search', validators = [DataRequired()])

@app.route("/search", methods=("GET","POST"))
def search():
	search = SearchForm()
	if search.validate_on_submit():
		# recupère donnée
		rech = search.recherche.data
		if rech != None:
			albums = AlbumSearch(rech)
			artist = ArtistSearch(rech)
			songwritters = SongwriterSearch(rech)
			genre = GenreSearch(rech)
			return render_template("search.html", 
				artist = artist,
				songwritters = songwritters,
				genre = GenreSearch(rech),
				albums = AlbumSearch(rech))
	else:
		return render_template("search.html", 
			artist = "",
			songwritters = "",
			genre = "",
			albums = "")


# **************************
# Playlist
# **************************
class NewPlaylist(Form):
	title = StringField('Title', validators=[DataRequired()])


@app.route("/save/playlist", methods=("GET","POST"))
@login_required
def save_playlist():
	f = NewPlaylist()
	if f.validate_on_submit():
		playlist = Playlist(id = create_playlist_id(),
							user_id = current_user.username,
							title = f.title.data)
		db.session.add(playlist)
		db.session.commit()
	return render_template("user_page.html",
							playlist = get_playlistUser(current_user),
							form = NewPlaylist(),
							artF = CreateArtist(),
							swF = CreateSW(),
							genF = CreateGenre(),
							albF = CreateAlbum())


@app.route("/adding/album/<int:id>/<int:aid>", methods=("GET","POST"))
@login_required
def add_album_to_playlist(id,aid):
	a = get_album(aid)
	p = getPlaylist(id,current_user)
	p.alb.append(a)
	db.session.commit()
	return render_template("one_playlist.html",
							playlist = p)

@app.route("/remove/album/<int:id>/<int:aid>", methods=("GET","POST"))
@login_required
def remove_album_from_playlist(id,aid):
	a = get_album(aid)
	p = getPlaylist(id,current_user)
	p.alb.remove(a)
	db.session.commit()
	return render_template("user_page.html",
							form = NewPlaylist(),
							artF = CreateArtist(),
							swF = CreateSW(),
							genF = CreateGenre(),
							albF = CreateAlbum())


@app.route("/playlist/<string:username>/<int:id>", methods=("GET","POST"))
def one_playlist(username, id):
	return render_template("one_playlist.html", 
							playlist = getPlaylist(id,current_user))



@app.route("/playlist/suppr/<int:id>", methods=("GET","POST"))
def deletePlaylist(id):
	delPlaylist(id, current_user)
	return render_template("user_page.html",
							form = NewPlaylist(),
							artF = CreateArtist(),
							swF = CreateSW(),
							genF = CreateGenre(),
							albF = CreateAlbum())



# **************************
# User
# **************************

@app.route("/user", methods=("GET","POST"))
@login_required
def user_page():
	return render_template("user_page.html",
							playlist = get_playlistUser(current_user),
							form = NewPlaylist(),
							artF = CreateArtist(),
							swF = CreateSW(),
							genF = CreateGenre(),
							albF = CreateAlbum())