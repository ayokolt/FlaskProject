from flask import Flask
from flask.ext.script import Manager
from flask.ext.bootstrap import Bootstrap
import os.path
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager

app = Flask(__name__)
app.debug = True
app.config['BOOTSTRAP_SERVE_LOCAL'] = True

manager = Manager(app)

Bootstrap(app)

def mkpath(p):
	return os.path.normpath(os.path.join(os.path.dirname(__file__),p))

app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('../spotifake.db'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db = SQLAlchemy(app)

app.config['SECRET_KEY'] = "e91697d3-cb1d-4487-a091-b589de59b8e0"

login_manager = LoginManager(app)
login_manager.login_view = "login"