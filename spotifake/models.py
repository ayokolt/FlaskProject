import yaml, os.path
from .app import db, login_manager
from flask.ext.login import UserMixin
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy import func


# **************************
# MANY-TO-MANY TABLES
# **************************


# Albums/Playlists
albus = db.Table('albus', 
	db.Column('playlist_id', db.Integer, db.ForeignKey('playlist.id')),
	db.Column('album_id', db.Integer, db.ForeignKey('album.id')))

# Genres/Albums
genres = db.Table('genres', 
	db.Column('genre_id', db.Integer, db.ForeignKey("genre.id")),
	db.Column('album_id', db.Integer, db.ForeignKey("album.id")))



# **************************
# ARTIST
# **************************

class Artist(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(50))

	def __repr__(self):
		return "<Artist (%d) %s>" % (self.id, self.name)

def get_artist(id):
	# returns the artist matching the id
	return Artist.query.get(id)


def get_all_artists():
	# returns all artists
	return Artist.query.all()

def get_artistAlbums(id):
	# returns albums related to an artist matching its id
	return Album.query.filter(Album.artist_id == id).all()

def del_artist(id):
	a = get_artist(id)
	liste = get_artistAlbums(id)
	for al in liste:
		al.artist_id = 1
	db.session.delete(a)
	db.session.commit()


def get_artistAlbumsNB(id):
	# returns albums related to an artist matching its id
	return Album.query.filter(Album.artist_id == id).all().count()

def create_artist_id():
	a = Artist.query.count()
	if a == 0:
		return 1
	else:
		a = db.session.query(func.max(Artist.id)) # retourne une query de tuple
		return a[0][0] + 1


# **************************
# SONGWRITER
# **************************

class Songwriter(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(50))

def get_all_sw():
	return Songwriter.query.all()

def get_sw(id):
	# returns a songwriter matching the id
	return Songwriter.query.get(id)

def del_sw(id):
	a = get_sw(id)
	liste = get_swAlbums(id)
	for al in liste:
		al.sw_id = 1
	db.session.delete(a)
	db.session.commit()

def get_swAlbums(id):
	# returns albums related to an artist matching its id
	return Album.query.filter(Album.sw_id == id).all()


# Compositeur
def get_songwritter(id):
	return Album.query.get(id)

def get_all_sw():
	return Songwriter.query.all()

def create_songwriter_id():
	a = Songwriter.query.count()
	if a == 0:
		return 1
	else:
		a = db.session.query(func.max(Songwriter.id)) # retourne une query de tuple
		return a[0][0] + 1



# **************************
# GENRE
# **************************

class Genre(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(30))
	album = db.relationship('Album', secondary=genres, backref=db.backref('genres', lazy='dynamic'))

	def __repr__(self):
		return "%s" % (self.name)

def get_genre(id):
	# returns a songwriter matching the id
	return Genre.query.get(id)
# Neccessaire à la relation Album et Genre
def getGenre(name):
	return Genre.query.filter_by(name=name).first()

def get_all_genre():
	return Genre.query.all()

def del_genre(id):
	supp = get_genre(id)
	for x in supp.album:
		supp.genre.remove(x)
	db.session.delete(supp)
	db.session.commit()


# **************************
# ALBUM
# **************************

class Album(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(100))
	release_year = db.Column(db.Integer)
	artist_id = db.Column(db.Integer, db.ForeignKey("artist.id")) 
	sw_id = db.Column(db.Integer, db.ForeignKey("songwriter.id"))
	img = db.Column(db.String(200))
	genre = db.relationship('Genre', secondary=genres, backref=db.backref('albums', lazy='dynamic'))
	

	def __repr__(self):
		return "<Album (%d) %s %s %s >" % (self.id, self.title, self.release_year, self.genre)


# Album
def get_album(id):
	# returns the album matching the id
	return Album.query.get(id)

def get_all_albums():
	# returns all albums
	return Album.query.all()

def delAlbum(id):
	supp = get_album(id)
	for x in supp.genre:
		supp.genre.remove(x)
	db.session.delete(supp)
	db.session.commit()


# **************************
# PLAYLIST
# **************************


class Playlist(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column(db.String(50), db.ForeignKey("user.username"), primary_key = True)
	title = db.Column(db.String(100))
	alb = db.relationship('Album', secondary=albus,backref=db.backref('playlists', lazy='dynamic'))
		
	def __repr__(self):
		return "<Playlist Object (%d) %s >" % (self.id, self.title)

def create_playlist_id():
	a = Playlist.query.count()
	if a == 0:
		return 1
	else:
		a = db.session.query(func.max(Playlist.id)) # retourne une query de tuple
		return a[0][0] + 1

def PlaylistSearch(letter):
	return Playlist.query.filter(Playlist.title.like('%'+letter+'%')).all()

def get_nbAlbumsPlaylist(id, user):
	return getPlaylist(id, user).alb.count()

# Neccessaire à la relation Album et Playlist + Méthode
def get_all_playlist():
	return Playlist.query.all()

def getPlaylist(id, user):
	return Playlist.query.get((id,user.username))

def get_albumPlaylist(id):
	ens = set()
	a = getPlaylist(id)
	for elem in a.albu:
		ens.append(elem)
	return ens

def get_playlistUser(user):
	return Playlist.query.filter_by(user_id = user.username).all()

def addPlaylist(title,user):
	addPl = Playlist(title = title, user = user)
	db.session.add(addPl)
	db.session.commit()

	
# manque ajouter album dans playlist et le supprimer

def getAlbumInPlaylist(id):
	see = getPlaylist(id)
	ens = set()
	for alb in see:
		ens.append(alb)
	return ens

def addAlbumInPlaylist(id, nomAlb):
	addAl = getPlaylist(id)
	addAl.albus.append(nomAlb)
	db.session.add(addPl)
	db.session.commit()

def delAlbumInPlaylist(id):
	suppAl = getPlaylist(id)
	db.session.delete(suppAl)
	db.session.commit()

def delPlaylist(id, user):
	supp = getPlaylist(id, user)
	for x in supp.alb:
		supp.alb.remove(x)
	db.session.delete(supp)
	db.session.commit()


# **************************
# USER
# **************************


class User(db.Model, UserMixin):
	username = db.Column(db.String(50), primary_key = True)
	password = db.Column(db.String(64))
	role = db.Column(db.String(6))
	playlist = db.relationship('Playlist', backref='user', lazy= 'dynamic')

	def get_id(self):
		return self.username

@login_manager.user_loader
def load_user(username):
	# if username!="":
	return User.query.get(username)


def existing_user(username):
	# verification si l'username existe déjà
	return db.session.query(User.username).filter_by(username = username).scalar()

def god_user(user):
	return user.role == "admin"



# **************************
# autres
# **************************


def get_sample():
	return Album.query.limit(5).all()

def ArtistSearch(letter):
	return Artist.query.filter(Artist.name.like('%'+letter+'%')).all()

def SongwriterSearch(letter):
	return Songwriter.query.filter(Songwriter.name.like('%'+letter+'%')).all()

def GenreSearch(letter):
	return Genre.query.filter(Genre.name.like('%'+letter+'%')).all()
	
def AlbumSearch(letter):
	return Album.query.filter(Album.title.like('%'+letter+'%')).all()



	## in DB

def inDBArtist(name):
	liste = get_all_artists()
	i = 0
	found = False
	while i<len(liste) and not found:
		if liste[i].name.lower() == name.lower():
			found = True
		i+=1
	return found

def inDBSW(name):
	liste = get_all_sw()
	i = 0
	found = False
	while i<len(liste) and not found:
		if liste[i].name.lower() == name.lower():
			found = True
		i+=1
	return found


def inDBGenre(name):
	liste = get_all_genre()
	i = 0
	found = False
	while i<len(liste) and not found:
		if liste[i].name.lower() == name.lower():
			found = True
		i+=1
	return found


def inDBAlbums(name):
	liste = get_all_albums()
	i = 0
	found = False
	while i<len(liste) and not found:
		if liste[i].title.lower() == name.lower():
			found = True
		i+=1
	return found

