from .app import manager, db
import yaml
# importe des modèles
from .models import *
from hashlib import sha256

@manager.command
def loaddb(filename):
	db.create_all()
	albumsFic = yaml.load(open(filename))

	ghostA = Artist(name="unknown")
	ghostSW = Songwriter(name="unknown")
	db.session.add(ghostA)
	db.session.add(ghostSW)
	db.session.commit()

	artistSave = {}
	swSave = {}

	# Artiste + Compositeur
	for albumInfos in albumsFic:
		artist = albumInfos["by"]
		sw = albumInfos["parent"]
		if artist not in artistSave:
			aO = Artist(name=artist)
			db.session.add(aO)
			artistSave[artist] = aO
		if sw not in swSave:
			swO = Songwriter(name=sw)
			db.session.add(swO)
			swSave[sw] = swO
	db.session.commit()


# implementation neccessaire pour la relation
	# Genre
	genreAll = set()
	for gen in albumsFic:
		a = gen["genre"]
		for genre in a:
			if genre not in genreAll:
				o = Genre(name=genre)
				genreAll.add(o.name)
				db.session.add(o)
	db.session.commit()

	# Albums
	for albumInfos in albumsFic:
		a = artistSave[albumInfos["by"]]
		sw = swSave[albumInfos["parent"]]
		o = Album(title		   = albumInfos["title"],
				  release_year = albumInfos["releaseYear"],
				  artist_id	   = a.id,
				  sw_id		   = sw.id,
				  img		   = albumInfos["img"])
		listGenre = albumInfos["genre"]
		#Genres
		for elem in listGenre:
			gO = getGenre(elem)
			o.genre.append(gO)
		db.session.add(o)
	db.session.commit()




			


@manager.command
def syncdb():
	db.create_all()

@manager.command
def newuser(username, password):
	m = sha256()
	m.update(password.encode())
	u = User(username = username, password = m.hexdigest(), role = "lambda")
	db.session.add(u)
	db.session.commit()

@manager.command
def goduser(username, password):
	m = sha256()
	m.update(password.encode())
	u = User(username = username, password = m.hexdigest(), role = "admin")
	db.session.add(u)
	db.session.commit()

@manager.command
def passwd(username, password):
	m = sha256()
	m.update(password.encode())
	u = User.query.filter(User.username == username)
	u.password = password
	db.session.commit()