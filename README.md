===============================================================================
INSTALLATION
===============================================================================
	
	If you need a virtualenv :
		$ virtualenv -p python3 venv
  		$ source venv/bin/activate

  	else:
  		GOTO Module to be installed

  	LABEL : Module to be installed
  		$ sudo apt-get install python-pip 
  		$ pip3 install flask
 		$ pip3 install Flask-Script
 		$ pip3 install Flask-Bootstrap
 		$ pip3 install Flask-Sqlachemy
 		$ pip3 install Flask-Login
 		$ pip3 install Flask-Wtf

===============================================================================
RUNNING
===============================================================================

	$ ./manage.py runserver

===============================================================================
USING
===============================================================================

  	direct your browser to http://localhost:5000/
  	The server actually listens on all interfaces.